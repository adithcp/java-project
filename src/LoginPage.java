import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

public class LoginPage implements ActionListener{
	
	JFrame frame = new JFrame();
    JButton loginButton = new JButton("Login");
	JButton resetButton = new JButton("Reset");

	JTextField userIDField = new JTextField();
	JPasswordField userPasswordField = new JPasswordField();

	JLabel userIDLabel = new JLabel("userID:");
	JLabel userPasswordLabel = new JLabel("password:");
	JLabel messageLabel = new JLabel();
	

	LoginPage(){
		userIDLabel.setBounds(50,100,75,25);
		userPasswordLabel.setBounds(50,150,75,25);

		messageLabel.setBounds(125,250,250,35);
		messageLabel.setFont(new Font(null,Font.ITALIC,25));

		userIDField.setBounds(125,100,200,25);
		userPasswordField.setBounds(125,150,200,25);

		loginButton.setBounds(110,200,100,25);
		loginButton.setFocusable(false);
		loginButton.addActionListener(this);

		resetButton.setBounds(230,200,100,25);
		resetButton.setFocusable(false);
		resetButton.addActionListener(this);

		frame.add(userIDLabel);
		frame.add(userPasswordLabel);
		frame.add(messageLabel);
		frame.add(userIDField);
		frame.add(userPasswordField);
		frame.add(loginButton);
		frame.add(resetButton);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(420,420);
		frame.setLocation(500, 200);
		frame.setLayout(null);
		frame.setVisible(true);

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==resetButton) {

			userIDField.setText("");
			userPasswordField.setText("");

		}

		if(e.getSource()==loginButton) {
			String userID = userIDField.getText();
			String password = String.valueOf(userPasswordField.getPassword());

			//check user in DB
			try {
				loadUserDetails(userID,password);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			 
			
		}
	}
	
	void loadUserDetails(String userID, String password) throws Exception {
		DBConnection connection = new DBConnection();
		//System.out.println("*****************************************");
		Connection con = connection.createConnection();
		String query = "SELECT * FROM login_data where User_name=? and Password=?";
		
		// create JDBC statement object
		PreparedStatement st = con.prepareStatement(query);
        st.setString(1, userID);
        st.setString(2, password);
        
        // send and execute SQL query in Database software
        ResultSet rs = st.executeQuery();
        boolean flag = false;
        
        // process the ResultSet object
	    while (rs.next()) {
	    	flag = true;
	        //System.out.println(rs.getInt(1) + " " + rs.getString(2));
	    }
	    if (flag == true) {
	        //System.out.println("\nUser found.");
	        frame.dispose();
			WelcomePage welcomePage = new WelcomePage(userID);
	    } else {
	        messageLabel.setForeground(Color.red);
			messageLabel.setText("username not found");
	    }
	    
	   // close JDBC objects
       rs.close();
	   st.close();
	   con.close();
		
	}
}


