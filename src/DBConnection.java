import java.sql.Connection;
import java.sql.DriverManager;

public class DBConnection {
	
	Connection createConnection() throws Exception {
		
		// register MySQL thin driver with DriverManager service
	    Class.forName("com.mysql.cj.jdbc.Driver");
	
	    // variables	
	    final String url = "jdbc:mysql:///u_login";
	    final String user = "PRD_DB";
	    final String password = "Root@123";
	
	    // establish the connection
	    Connection con = DriverManager.getConnection(url, user, password);
	    
	    if (con == null) {
	       System.out.println("JDBC connection is not established");
	       return null;  
	    } else
	       System.out.println("Congratulations," + 
	            " JDBC connection is established successfully.\n");
	    return con;
	}
}
