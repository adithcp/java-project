import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;


public class WelcomePage implements ActionListener{
	JFrame frame = new JFrame();
	JLabel welcomeLabel = new JLabel();
	
	JTextField firstnamefield=new JTextField();
	JTextField lastnamefield=new JTextField();
	JTextField mobilenumberfield=new JTextField();
	JTextField addressfield=new JTextField();
	JTextField cityfield=new JTextField();
	JTextField statefield=new JTextField();
	JTextField countryfield=new JTextField();
	JLabel firstname=new JLabel("First Name:");
	JLabel lastname=new JLabel("last name:");
	JLabel mobilenumber=new JLabel("mobile number:");
	JLabel address=new JLabel("address:");
	JLabel city=new JLabel("city:");
	JLabel state=new JLabel("state:");
	JLabel country=new JLabel("country:");
	JLabel message=new JLabel();
	
	JButton submit=new JButton("Submit");
	JButton reset=new JButton("Reset");
	
	JTextArea screen=new JTextArea();
	JCheckBox term=new JCheckBox("Accept Terms And Conditions.");
	
	
		
	WelcomePage(String userID){

		welcomeLabel.setBounds(0,0,200,35);
		welcomeLabel.setFont(new Font(null,Font.PLAIN,25));
		//welcomeLabel.setText("Hello "+userID);
		
        firstname.setBounds(20, 20, 100, 25);
        firstnamefield.setBounds(140, 20, 100, 25);
        frame.add(firstname);
        frame.add(firstnamefield);
        
        lastname.setBounds(20, 60, 100, 25);
        lastnamefield.setBounds(140, 60, 100, 25);
        frame.add(lastname);
        frame.add(lastnamefield);
        
        mobilenumber.setBounds(20, 100, 120, 25);
        mobilenumberfield.setBounds(140, 100, 120, 25);
        frame.add(mobilenumber);
        frame.add(mobilenumberfield);
        
        address.setBounds(20, 140, 100, 25);
        addressfield.setBounds(140, 140, 100, 25);
        frame.add(address);
        frame.add(addressfield);
        
        city.setBounds(20, 180, 100, 25);
        cityfield.setBounds(140, 180, 100, 25);
        frame.add(city);
        frame.add(cityfield);
        
        state.setBounds(20, 220, 100, 25);
        statefield.setBounds(140, 220, 100, 25);
        frame.add(state);
        frame.add(statefield);
        
        country.setBounds(20, 260, 100, 25);
        countryfield.setBounds(140, 260, 100, 25);
        frame.add(country);
        frame.add(countryfield);
        

        submit.setBounds(40,350,100,25);
        submit.setFocusable(false);
        submit.addActionListener(this);
        frame.add(submit);

        reset.setBounds(160,350,100,25);
        reset.setFocusable(false);
        reset.addActionListener(this);
        frame.add(reset);
        
        screen.setBounds(340, 30, 300, 400);
        screen.setLineWrap(true);
        screen.setEditable(false);
        frame.add(screen);
        
        message.setBounds(30, 380, 280, 25);
        frame.add(message);
        
        term.setBounds(20, 310, 250, 20);
        frame.add(term);
        
		frame.add(welcomeLabel);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(700, 500);
		frame.setLocation(400, 150);
		frame.setLayout(null);
		frame.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
	       if (e.getSource() == submit) {
	            if (term.isSelected()) {
	                //String data1;
	                String data
	                    = "firstName : "
	                      + firstnamefield.getText() + "\n"
	                      + "lastname : "
	                      + lastnamefield.getText() + "\n"
	                      + "Mobile : "
	                      + mobilenumberfield.getText() + "\n";
	                
	                String data2 = "Address : " + addressfield.getText();
	                
	                String data3
                    = "city : "
  	                      + cityfield.getText() + "\n"
  	                      + "state"
  	                      + statefield.getText() + "\n"
  	                      + "country : "
  	                      + countryfield.getText() + "\n";
  	                
	                screen.setText(data + data2 + data3);
	                screen.setEditable(false);
	                message.setText("Registration Successfully..");
	            }
	            else {
	                screen.setText("");
	                message.setText("Please accept the"
	                            + " terms & conditions..");
	            }
	        }
	 
	        else if (e.getSource() == reset) {
	            String def = "";
	            firstnamefield.setText(def);
	            lastnamefield.setText(def);
	            mobilenumberfield.setText(def);
	            addressfield.setText(def);
	            cityfield.setText(def);
	            statefield.setText(def);
	            countryfield.setText(def);
	            
	        }
		}
		
	}


